import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ErasableInputComponent } from './erasable-input.component';

describe('ErasableInputComponent', () => {
	let component: ErasableInputComponent;
	let fixture: ComponentFixture<ErasableInputComponent>;

	beforeEach(async(() => {
		TestBed.configureTestingModule({
			declarations: [ErasableInputComponent],
		}).compileComponents();
	}));

	beforeEach(() => {
		fixture = TestBed.createComponent(ErasableInputComponent);
		component = fixture.componentInstance;
		fixture.detectChanges();
	});

	it('should create', () => {
		expect(component).toBeTruthy();
	});
});
