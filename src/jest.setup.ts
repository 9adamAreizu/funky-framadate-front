import 'jest-preset-angular';

// DEFAULT GLOBAL MOCKS
Object.defineProperty(window, 'CSS', { value: null });
Object.defineProperty(document, 'doctype', {
	value: '<!DOCTYPE html>',
});
Object.defineProperty(window, 'getComputedStyle', {
	value: () => {
		return {
			display: 'none',
			appearance: ['-webkit-appearance'],
		};
	},
});
/**
 * ISSUE: https://github.com/angular/material2/issues/7101
 * Workaround for JSDOM missing transform property
 */
Object.defineProperty(document.body.style, 'transform', {
	value: () => {
		return {
			enumerable: true,
			configurable: true,
		};
	},
});

// PROMISE
Object.defineProperty(global, 'Promise', { writable: false, value: global.Promise });

// STORAGE
const mock = () => {
	let storage = {};
	return {
		getItem: (key: string): string | null => (key in storage ? storage[key] : null),
		setItem: (key: string, value: string): void => {
			storage[key] = value || '';
		},
		removeItem: (key: string): void => {
			delete storage[key];
		},
		clear: (): void => {
			storage = {};
		},
	};
};

Object.defineProperty(window, 'localStorage', { value: mock() });
Object.defineProperty(window, 'sessionStorage', { value: mock() });
Object.defineProperty(window, 'getComputedStyle', { value: () => ['-webkit-appearance'] });
